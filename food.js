
const nutriment = (plat, nutriment) => parseFloat(
  Array.from(plat.querySelectorAll('dt'))  // find all dt's in plat
    .find(el => el.innerHTML.toLowerCase() === nutriment) // keep the one with the nutriment
    .nextElementSibling // get to the dd
    .innerHTML) 

const glucides = plat => nutriment(plat, 'glucides')
const lipides = plat => nutriment(plat, 'lipides')
const calories = plat => nutriment(plat, 'calories')
const proteines = plat => nutriment(plat, 'protéines')

const platEl2js = (plat) => ({
  name: plat.querySelector('p').innerHTML,
  weight: parseFloat(plat.attributes['data-weight'].value),
  caloriesTotal: parseFloat(plat.attributes['data-calories'].value),
  calories: calories(plat),
  lipides: lipides(plat),
  glucides: glucides(plat),
  proteines: proteines(plat)
})

const plats = Array.from(document.querySelectorAll('.plat')).map(platEl2js)

const plat2tr = p =>
      `<tr>
     <td>${p.name}</td><td>${p.calories}</td><td>${p.glucides}</td>
   </tr>
`
const plats2table = (plats) => {
  const table = document.createElement('table')
  table.id="foodlist"
  table.innerHTML = plats.map(plat2tr).join('\n')
  return table
}

const redraw = el => {
  document.querySelector('#foodlist').replaceWith(el)
}

// key t: redraw with table view
document.addEventListener('keyup', e => {
  if (e.key === 't')
    redraw(plats2table(plats))
})

